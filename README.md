# MyFlutterPod

[![CI Status](https://img.shields.io/travis/OmgKevin/MyFlutterPod.svg?style=flat)](https://travis-ci.org/OmgKevin/MyFlutterPod)
[![Version](https://img.shields.io/cocoapods/v/MyFlutterPod.svg?style=flat)](https://cocoapods.org/pods/MyFlutterPod)
[![License](https://img.shields.io/cocoapods/l/MyFlutterPod.svg?style=flat)](https://cocoapods.org/pods/MyFlutterPod)
[![Platform](https://img.shields.io/cocoapods/p/MyFlutterPod.svg?style=flat)](https://cocoapods.org/pods/MyFlutterPod)

## Example

 flutter_moudle 模块通过Git托管

## Requirements

## Installation

MyFlutterPod is available through [CocoaPods](https://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
  pod 'MyFlutterPod',:git=>'https://gitlab.com/OmgKevin/myflutterpod.git'
```


## License

MyFlutterPod is available under the MIT license. See the LICENSE file for more info.
